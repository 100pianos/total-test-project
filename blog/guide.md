---
# layout: page
title: Style Guide
permalink: /style-guide/
---

# Table of Contents

* [Typography](#typography)
   * [Fonts](#fonts)
   * [Headings](#headings)
   * [Body Copy](#body-copy)
      * [Inline Formatting](#inline-formatting)
      * [Lists](#lists)
      * [Links and Buttons](#links-and-buttons)
      * [Alerts](#alerts)
      * [Tables](#tables)
* [Images](#images)
* [Colors](#colors)
   * [Gradients](#gradients)
* [Page Layout](#page-layout)

-----------------------------------------

## Typography

When it's used thoughtfully, typography is a powerful tool that can add visual meaning to what is communicated. Using type thoughtfully is vital to keeping our website designs looking professional. Please use the following guidelines when formatting your textual content.

### Fonts

PT Sans is the only font used on the Rincon website. PT Sans is an open-source sans-serif font. This font is extremely versatile, with six weights, all with matching italics and small caps variants. Below is a sample of the font:

#### Uppercase
{: style='text-align:center'}

A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
{: style='font-size: 200%; text-align:center;'}

#### Lowercase
{: style='text-align:center;'}

a b c d e f g h i j k l m n o p q r s t u v w x y z
{: style='font-size: 200%; text-align:center;'}

#### Numerals and Punctuation

0 1 2 3 4 5 6 7 8 9
{: style='font-size: 200%; text-align:center;'}

! @ # $ % ^ & * ~
{: style='font-size: 200%; text-align:center;'}

( { [ < ” – + = . , ; : ” > ] } )
{: style='font-size: 200%; text-align:center;'}

### Headings

# H1. Level 1 Heading
{: .text-center}

## H2. Level 2 Heading
{: .text-center}

### H3. Level 3 Heading
{: .text-center}

#### H4. Level 4 Heading
{: .text-center}

##### H5. Level 5 Heading
{: .text-center}

###### H6. Level 6 Heading
{: .text-center}

-----------

### Body Copy

All body copy on the website is rendered using the font family PT Sans. Line height is set at 142% of the character width for optimal readability.

#### Inline Formatting

Below are some textual samples demonstrating the inline formatting styles available on the website.

> **Example bold text**

> _Example italic text_

> **_Example bold and italic text_**

> ~~Example strikethrough text~~


> `Example code text`

In addition to the inline styles listed above, the site also supports block quotes, as demonstrated below:

> This is an example of a block quote. Block quotes should
> only be used when quoting something from another source.

<div class='row' markdown='1'>

* Unordered Lists (default bullets)
* List Item 2
* List Item 3
* List Item 4
* List Item 5
{: .col-md-3}

* Nested Unordered Lists
* List Item 2 – This bullet is longer than usual but should wrap to the next line without any problems.
   * List Item 2.1
   * List Item 2.2
      * List Item 2.2.1 – This subbullet is also longer than usual but should wrap to the next line without any problems.
   * List Item 2.2.2
   * List Item 2.2.3
* List Item 3
{: .col-md-3}

1. Ordered Lists (default bullets)
2. List Item 2
3. List Item 3
4. List Item 4
5. List Item 5
{: .col-md-3}

1. Ordered Lists (lower alpha)
2. List Item 2
3. List Item 3
4. List Item 4
5. List Item 5
{: .col-md-3 style='list-style-type: lower-alpha;'}
</div>

<div class='row' markdown='1'>
1. Ordered Lists (lower greek)
2. List Item 2
3. List Item 3
4. List Item 4
5. List Item 5
{: .col-md-3 style='list-style-type: lower-greek;'}

1. Ordered Lists (lower roman)
2. List Item 2
3. List Item 3
4. List Item 4
5. List Item 5
{: .col-md-3 style='list-style-type: lower-roman;'}

1. Ordered Lists (upper alpha)
2. List Item 2
3. List Item 3
4. List Item 4
5. List Item 5
{: .col-md-3 style='list-style-type: upper-alpha;'}

1. Ordered Lists (upper roman)
2. List Item 2
3. List Item 3
4. List Item 4
5. List Item 5
{: .col-md-3 style='list-style-type: upper-roman;'}

</div>

While not shown above, you can also nest ordered lists.

#### Links and Buttons

Hyperlinks are formatted as blue text without any text decoration (underlining, etc.). When users hover their mouse over a link (or focus on a link using the keyboard), the link will be underlined, indicating that it can be followed.

When linking to third-party websites (i.e., anything not on the Rincon website), be sure to specify that the link should open in a new tab. This is done by clicking on the settings icon after the link has been created.

In addition to the standard hyperlink styles, the Rincon website also includes button formatting for hyperlinks. There are six different button styles available on our website. These should only be used for hyperlinks or other interactive elements, as they indicate that the element is “clickable”. Additionally, they should be used sparingly as they have a much greater visual weight and could impact usability and readability if overused. The available buttons styles are as follows:

<div class='row' markdown='1'>
[Default Button](#){: .btn .btn-default} [Primary Button](#){: .btn .btn-primary} [Info Button](#){: .btn .btn-info} [Success Button](#){: .btn .btn-success} [Warning Button](#){: .btn .btn-warning} [Danger Button](#){: .btn .btn-danger}
</div>

#### Alerts

We have the ability to format a paragraph (or paragraphs) as an alert. These are useful for alerting website visitors about time-sensitive information and should NOT be used for static content. The available alert styles are listed below:

<div class='row' markdown='1'>
*Info Alert*\\
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lobortis
consequat dolor, vel ornare mauris consectetur et. Cras eget sem in nunc
interdum aliquet vel quis arcu.
{: .alert .alert-info .col-md-3}

*Success Alert*\\
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lobortis
consequat dolor, vel ornare mauris consectetur et. Cras eget sem in nunc
interdum aliquet vel quis arcu.
{: .alert .alert-success .col-md-3}

*Warning Alert*\\
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lobortis
consequat dolor, vel ornare mauris consectetur et. Cras eget sem in nunc
interdum aliquet vel quis arcu.
{: .alert .alert-warning .col-md-3}

*Danger Alert*\\
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lobortis
consequat dolor, vel ornare mauris consectetur et. Cras eget sem in nunc
interdum aliquet vel quis arcu.
{: .alert .alert-danger .col-md-3}
</div>


| Countries | Capitals        | Population  | Language |
|-----------+-----------------+-------------+----------|
| USA       | Washington D.C. | 309 million | English  |
| Sweden    | Stockholm       | 9 million   | Swedish  |
{: style='margin: 1em auto;' border='1'}


## Images

When adding images to the Rincon website, it is recommended that you resize
the images before uploading them to the website. This will ensure that users
viewing the website will see an image optimized for web viewing. In general,
adhere to the following guidelines when creating and uploading images for the
Rincon website:

* Use appropriate formats for images based on the content of the image.
   * Photographs should be in JPG format.
   * Graphics, charts, and similar created content should be in PNG or SVG format.
   * GIF images should only be used if the same image is not available as a PNG (or
     if the filesize of the PNG version is significantly larger).
   * BMP, TIFF, and other image formats should **NOT** be used.
* When possible, resize image dimensions to with 1000 pixels wide and 1000 pixels tall.
* When possible, optimize images to reduce the filesize to no greater than 100 KB.

## Colors

Below is the primary color palette used the Rincon website. Color names, hexadecimal codes, and RGB values are listed for each color:

<div class='row' markdown='1'>
**Orient**\\
#00587d\\
rgb(0,88,125)
{: .col-md-2 .style-guide-color-block .col-md-offset-1 style='background-color: #00587d;'}

**Ocean Green**\\
#399c77\\
rgb(57,159,119)
{: .col-md-2 col-md-offset-1 .style-guide-color-block style='background-color: #399c77;'}

**Tundora**\\
#444444\\
rgb(68,68,68)
{: .col-md-2 col-md-offset-1 .style-guide-color-block style='background-color: #444444;'}

**Chalice**\\
#aaaaaa\\
rgb(170,170,170)
{: .col-md-2 col-md-offset-1 .style-guide-color-block style='background-color: #aaaaaa;'}

**White**\\
#ffffff\\
rgb(255,255,255)
{: .col-md-2 col-md-offset-1 .alt-color .style-guide-color-block style='background-color: #ffffff;'}
</div>

The following tints and shades, based on the primary palette, can also be used on the website:

<div class='row' markdown='1'>
**Orient, Darkest**\\
#02222c\\
rgb(2,34,44)
{: .col-md-2 .col-md-offset-1 .style-guide-color-block style='background-color: #02222c;'}

**Orient, Dark**\\
#204B60\\
rgb(32,75,96)
{: .col-md-2 .style-guide-color-block style='background-color: #204B60;'}

**Orient**\\
#00587d\\
rgb(0,88,125)
{: .col-md-2 .style-guide-color-block style='background-color: #00587d;'}

**Orient, Light**\\
#c2e1f0\\
rgb(194,225,240)
{: .col-md-2 .style-guide-color-block .alt-color style='background-color: #c2e1f0;'}

**Orient, Lighter**\\
#deeff7\\
rgb(222,239,247)
{: .col-md-2 .style-guide-color-block .alt-color style='background-color: #deeff7;'}
</div>

<div class='row' markdown='1'>
**Ocean Green, Darkest**\\
#0e251b\\
rgb(14,37,27)
{: .col-md-2 .col-md-offset-1 .style-guide-color-block style='background-color: #0e251b;'}

**Ocean Green, Dark**\\
#184030\\
rgb(24,64,48)
{: .col-md-2 .style-guide-color-block style='background-color: #184030;'}

**Ocean Green**\\
#399c77\\
rgb(57,159,119)
{: .col-md-2 .style-guide-color-block style='background-color: #399c77;'}

**Ocean Green, Light**\\
#c7eadc\\
rgb(199,234,220)
{: .col-md-2 .style-guide-color-block .alt-color style='background-color: #c7eadc;'}

**Ocean Green, Lighter**\\
#e2f4ed\\
rgb(226,244,237)
{: .col-md-2 .style-guide-color-block .alt-color style='background-color: #e2f4ed;'}
</div>

Lastly, we have a secondary palette which can be used when additional colors are required. These colors should only be used when the primary palette does not meet all of your color needs. One example would be a technical diagram which requires five colors to distinguish its individual components.

<div class='row' markdown='1'>
**Cinnamon**\\
#7d3f00\\
rgb(125, 63, 0)
{: .col-md-2 .col-md-offset-2 .style-guide-color-block style='background-color: #7d3f00;'}

**Olive**\\
#7d6400\\
rgb(125,100,0)
{: .col-md-2 .style-guide-color-block style='background-color: #7d6400;'}

**Laser**\\
#c5b66a\\
rgb(197,182,106)
{: .col-md-2 .style-guide-color-block style='background-color: #c5b66a;'}

**Porsche**\\
#e7a955\\
rgb(231,169,85)
{: .col-md-2 .style-guide-color-block style='background-color: #e7a955;'}
</div>

<div class='row' markdown='1'>
**Lily**\\
#c1a0ad\\
rgb(193,160,173)
{: .col-md-2 .col-md-offset-2 .style-guide-color-block style='background-color: #c1a0ad;'}

**Amethyst Smoke**\\
#9d83ab\\
rgb(157,131,171)
{: .col-md-2 .style-guide-color-block style='background-color: #9d83ab;'}

**Cabaret**\\
#d04d72\\
rgb(208,77,114)
{: .col-md-2 .style-guide-color-block style='background-color: #d04d72;'}

**Burnt Sienna**\\
#e78155\\
rgb(231,129,85)
{: .col-md-2 .style-guide-color-block style='background-color: #e78155;'}
</div>

Note the background gray color, in case you want the effect of a transparent background without the white pattern showing through.

<div class='row' markdown='1'>
**Whisper**\\
#EAEAEA\\
rgb(234, 234, 234)
{: .col-md-2 .col-md-offset-2  .alt-color .style-guide-color-block style='background-color: #EAEAEA;'}
</div>

### Gradients

Gradients are a simple and subtle way to add dimension to an element. That said, they can quickly become overused, given a website an unprofessional and even dated look. When adding gradients to the Rincon website, please only use the following color combinations:


<div class='row' markdown='1'>
**Orient &larr; Ocean Green**<br /><br />
{: .col-md-2  .style-guide-color-block style='background-image: linear-gradient(135deg, #00587d, #399c77);'}

**Orient → Orient Dark/Darker**
{: .col-md-2 .col-md-offset-2 .style-guide-color-block style='background-image: linear-gradient(135deg, #00587d, #204B60);'}

**Orient → Orient Light/Lighter**
{: .col-md-2 .style-guide-color-block style='background-image: linear-gradient(135deg, #00587d, #C2E1F0);'}

**Ocean Green → Ocean Green Dark/Darker**
{: .col-md-2 .style-guide-color-block style='background-image: linear-gradient(135deg, #399c77, #184030);'}

**Ocean Green → Ocean Green Light/Lighter**
{: .col-md-2 .style-guide-color-block style='background-image: linear-gradient(135deg, #399c77, #c7eadc);'}

**Tundora → Chalice**<br /><br />
{: .col-md-2 .style-guide-color-block style='background-image: linear-gradient(135deg, #444444, #aaaaaa);'}
</div>


## PAGE LAYOUT

The Rincon website design makes heavy use of the Bootstrap CSS framework. In
particular, we rely on the Bootstrap grid system for managing most of the
website layout. The Bootstrap grid system is a fully-responsive, fluid grid
based on a 12-column model. For more information about Bootstrap’s grid
system, you can visit [their online documentation](http://getbootstrap.com/css/#grid).

<div class="row style-guide-show-grid" markdown='1'>
1
{: .col-md-1 style='text-align: center'}
1
{: .col-md-1 style='text-align: center'}
1
{: .col-md-1 style='text-align: center'}
1
{: .col-md-1 style='text-align: center'}
1
{: .col-md-1 style='text-align: center'}
1
{: .col-md-1 style='text-align: center'}
1
{: .col-md-1 style='text-align: center'}
1
{: .col-md-1 style='text-align: center'}
1
{: .col-md-1 style='text-align: center'}
1
{: .col-md-1 style='text-align: center'}
1
{: .col-md-1 style='text-align: center'}
1
{: .col-md-1 style='text-align: center'}
</div>
<div class="row style-guide-show-grid" markdown='1'>
8
{: .col-md-8 style='text-align: center'}
4
{: .col-md-4 style='text-align: center'}
</div>
<div class="row style-guide-show-grid" markdown='1'>
3
{: .col-md-3 style='text-align: center'}
9
{: .col-md-9 style='text-align: center'}
</div>
<div class="row style-guide-show-grid" markdown='1'>
7
{: .col-md-7 style='text-align: center'}
5
{: .col-md-5 style='text-align: center'}
</div>
<div class="row style-guide-show-grid" markdown='1'>
4
{: .col-md-4 style='text-align: center'}
4
{: .col-md-4 style='text-align: center'}
4
{: .col-md-4 style='text-align: center'}
</div>

<div class="row style-guide-show-grid" markdown='1'>
6
{: .col-md-6 style='text-align: center'}
6
{: .col-md-6 style='text-align: center'}
</div>

Bootstrap’s grid system is fully responsive, meaning that it will automatically scale the webpage content based on the size of the screen on which it is being viewed. The Rincon website is designed to provide two views: a desktop view (screen width >= 769 pixels) and a mobile view (screen width <= 768 pixels). It is worth noting that most tablets (iPad, etc.) will render using the mobile view. To provide users with the best experience regardless of their screen size, it is recommended that you utilize the Bootstrap grid system as much as possible.

