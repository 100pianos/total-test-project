---
# layout: page
title: Markdown Cheatsheet
---

<style type='text/css'>
  h2 { margin-top: 2em !important; clear: both !important; }
  hr { border-color: #aaaaaa; }
</style>


This is intended as a quick reference for what Markdown via Git can do.

## Table of Contents
* [Headers](#headers)  
* [Emphasis](#emphasis)  
* [Lists](#lists)  
* [Links](#links)  
* [Images](#images)  
* [Code and Syntax Highlighting](#code)  
* [Tables](#tables)  
* [Blockquotes](#blockquotes)  
* [Inline HTML](#html)  
* [Horizontal Rule](#hr)  
* [Line Breaks](#lines)  
* [YouTube Videos](#videos)  

<a name="headers"/>

## Headers

<div class='row' markdown='1'>

```no-highlight
# H1
## H2
### H3
#### H4
##### H5
###### H6

Alternatively, for H1 and H2, an underline-ish style:

Alt-H1
======

Alt-H2
------
```
{: .col-md-6}

<div class='col-md-6' markdown='1'>
# H1
## H2
{: .text-center}

### H3
{: .text-center}

#### H4
{: .text-center}

##### H5
{: .text-center}

###### H6
{: .text-center}

Alternatively, for H1 and H2, an underline-ish style:

Alt-H1
======
{: .text-center}

Alt-H2
------
{: .text-center}
</div>
</div>

<a name="emphasis"/>

## Emphasis

<div class='row' markdown='1'>

```no-highlight
Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~
```
{: .col-md-6}

<div class='col-md-6' markdown='1'>
Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~
</div>
</div>

<a name="lists"/>

## Lists

<div class='row' markdown='1'>

(In this example, leading and trailing spaces are shown with with dots: ⋅)

```no-highlight
1. First ordered list item
2. Another item
⋅⋅* Unordered sub-list. 
1. Actual numbers don't matter, just that it's a number
⋅⋅1. Ordered sub-list
4. And another item.

⋅⋅⋅You can have properly indented paragraphs within list items. Notice the blank line above, and the leading spaces (at least one, but we'll use three here to also align the raw Markdown).

⋅⋅⋅To have a line break without a paragraph, you will need to use two trailing spaces.⋅⋅
⋅⋅⋅Note that this line is separate, but within the same paragraph.⋅⋅
⋅⋅⋅(This is contrary to the typical GFM line break behaviour, where trailing spaces are not required.)

* Unordered list can use asterisks
- Or minuses
+ Or pluses
```
{: .col-md-6}

<div class='col-md-6' markdown='1'>
1. First ordered list item
2. Another item
  * Unordered sub-list. 
1. Actual numbers don't matter, just that it's a number
  1. Ordered sub-list
4. And another item.

   You can have properly indented paragraphs within list items. Notice the blank line above, and the leading spaces (at least one, but we'll use three here to also align the raw Markdown).

   To have a line break without a paragraph, you will need to use two trailing spaces.  
   Note that this line is separate, but within the same paragraph.  
   (This is contrary to the typical GFM line break behaviour, where trailing spaces are not required.)

* Unordered list can use asterisks
- Or minuses
+ Or pluses
</div>
</div>

<a name="links"/>

## Links

<div class='row' markdown='1'>
There are two ways to create links.

```no-highlight
[I'm an inline-style link](https://www.google.com)

[I'm an inline-style link with title](https://www.google.com "Google's Homepage")

[I'm a reference-style link][Arbitrary case-insensitive reference text]

[I'm a relative reference to a repository file](../blob/master/LICENSE)

[You can use numbers for reference-style link definitions][1]

URLs in angle brackets will automatically get turned into links. 
For example, <http://www.example.com>.

Some text to show that the reference links can follow later.

[arbitrary case-insensitive reference text]: https://www.mozilla.org
[1]: http://slashdot.org
```
{: .col-md-6}

<div class='col-md-6' markdown='1'>
[I'm an inline-style link](https://www.google.com)

[I'm an inline-style link with title](https://www.google.com "Google's Homepage")

[I'm a reference-style link][Arbitrary case-insensitive reference text]

[I'm a relative reference to a repository file](../blob/master/LICENSE)

[You can use numbers for reference-style link definitions][1]

URLs in angle brackets will automatically get turned into links. 
For example, <http://www.example.com>.

Some text to show that the reference links can follow later.

[arbitrary case-insensitive reference text]: https://www.mozilla.org
[1]: http://slashdot.org
</div>
</div>

<a name="images"/>

## Images

<div class='row' markdown='1'>

```no-highlight
Here's our logo (hover to see the title text):

Inline-style: 
![alt text](http://www.rincon.com/wp-content/themes/rrc/imgs/logo-alt.svg "Logo Title Text 1")

Reference-style: 
![alt text][logo]

[logo]: http://www.rincon.com/wp-content/themes/rrc/imgs/logo-alt.svg "Logo Title Text 2"
```
{: .col-md-6}

<div class='col-md-6' markdown='1'>
Here's our logo (hover to see the title text):

Inline-style: 
![alt text](http://www.rincon.com/wp-content/themes/rrc/imgs/logo-alt.svg "Logo Title Text 1")

Reference-style: 
![alt text][logo]

[logo]: http://www.rincon.com/wp-content/themes/rrc/imgs/logo-alt.svg "Logo Title Text 2"
</div>
</div>

<a name="code"/>

## Code and Syntax Highlighting

<div class='row' markdown='1'>

Code blocks are part of the Markdown spec, but syntax highlighting isn't. However, many renderers -- like Github's and *Markdown Here* -- support syntax highlighting. Which languages are supported and how those language names should be written will vary from renderer to renderer. *Markdown Here* supports highlighting for dozens of languages (and not-really-languages, like diffs and HTTP headers); to see the complete list, and how to write the language names, see the [highlight.js demo page](http://softwaremaniacs.org/media/soft/highlight/test.html).
{: .col-md-12}

```no-highlight
Inline `code` has `back-ticks around` it.
```
{: .col-md-6}

Inline `code` has `back-ticks around` it.
{: .col-md-6}

Blocks of code are either fenced by lines with three back-ticks <code>```</code>, or are indented with four spaces. I recommend only using the fenced code blocks -- they're easier and only they support syntax highlighting if we decided that was something we wanted to implement.
{: .col-md-12}

<pre lang="no-highlight" class='col-md-6'><code>```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```
 
```python
s = "Python syntax highlighting"
print s
```
 
```
No language indicated, so no syntax highlighting. 
But let's throw in a &lt;b&gt;tag&lt;/b&gt;.
```
</code></pre>


<div class='col-md-6' markdown='1'>
```javascript
// Below is a small snippet of JavaScript.
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
# Below is a short snippet of Python.
s = "Python syntax highlighting"
print s
```

```
No language indicated, so no syntax highlighting. 
But let's throw in a <b>tag</b>.
```
</div>
</div>

<a name="tables"/>

## Tables

<div class='row' markdown='1'>

Tables aren't part of the core Markdown spec, but they are part of GFM and *Markdown Here* supports them. They are an easy way of adding tables to your email -- a task that would otherwise require copy-pasting from another application.

```no-highlight
Colons can be used to align columns.

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

There must be at least 3 dashes separating each header cell.
The outer pipes (|) are optional, and you don't need to make the 
raw Markdown line up prettily. You can also use inline Markdown.

Markdown | Less | Pretty
--- | --- | ---
*Still* | `renders` | **nicely**
1 | 2 | 3
```
{: .col-md-6}

<div class='col-md-6' markdown='1'>
Colons can be used to align columns.

| Tables        | Are           | Cool |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |
{:  border='1' }

There must be at least 3 dashes separating each header cell. The outer pipes (\|) are optional, and you don't need to make the raw Markdown line up prettily. You can also use inline Markdown.

Markdown | Less | Pretty
--- | --- | ---
*Still* | `renders` | **nicely**
1 | 2 | 3
{:  border='1' }

</div>
</div>

<a name="blockquotes"/>

## Blockquotes

<div class='row' markdown='1'>

```no-highlight
> Blockquotes are very handy in email to emulate reply text.
> This line is part of the same quote.

Quote break.

> This is a very long line that will still be quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a blockquote. 
```
{: .col-md-6 }

<div class='col-md-6' markdown='1'>
> Blockquotes are very handy in email to emulate reply text.
> This line is part of the same quote.

Quote break.

> This is a very long line that will still be quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a blockquote. 
</div>
</div>

<a name="html"/>

## HTML

You can also use raw HTML in your Markdown, and it'll mostly work pretty well. 

<div class='row' markdown='1'>

```no-highlight
<dl>
  <dt>Definition list</dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well by default. Use HTML <em>tags</em>.</dd>
</dl>

<div markdown='1'>
**Alternatively**, you can include Markdown in HTML if you add an
HTML element named `markdown` with a value of 1.
</div>

```
{: .col-md-6 }

<div class='col-md-6' markdown='1'>
<dl>
  <dt>Definition list</dt>
  <dd>Is something people use sometimes.</dd>

  <dt>Markdown in HTML</dt>
  <dd>Does *not* work **very** well by default. Use HTML <em>tags</em>.</dd>
</dl>

<div markdown='1'>
**Alternatively**, you can include Markdown in HTML if you add an
HTML element named `markdown` with a value of 1.
</div>

</div>
</div>

<a name="hr"/>

## Horizontal Rule

<div class='row' markdown='1'>

```
Three or more...

---

Hyphens

***

Asterisks

___

Underscores
```
{: .col-md-6 }

<div class='col-md-6' markdown='1'>
Three or more...

---

Hyphens

***

Asterisks

___

Underscores
</div>
</div>

<a name="lines"/>

## Line Breaks

<div class='row' markdown='1'>
My basic recommendation for learning how line breaks work is to experiment and discover -- hit &lt;Enter&gt; once (i.e., insert one newline), then hit it twice (i.e., insert two newlines), see what happens. You'll soon learn to get what you want. "Markdown Toggle" is your friend. 

Here are some things to try out:

<div class='row' markdown='1'>
```
Here's a line for us to start with.

This line is separated from the one above by two newlines, so it will be a *separate paragraph*.

This line is also a separate paragraph, but...
This line is only separated by a single newline, so it's a separate line in the *same paragraph*.
```
{: .col-md-6 }

<div class='col-md-6' markdown='1'>
Here's a line for us to start with.

This line is separated from the one above by two newlines, so it will be a *separate paragraph*.

This line is also begins a separate paragraph, but...  
This line is only separated by a single newline, so it's a separate line in the *same paragraph*.

</div>
</div>


<a name="videos"/>

## Videos

<div class='row' markdown='1'>

They can't be added directly but you can add an image with a link to the video like this:

<div class='col-md-6' markdown='1'>
```no-highlight
<a href="http://www.youtube.com/watch?feature=player_embedded&v=YOUTUBE_VIDEO_ID_HERE" target="_blank"><img src="http://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg" 
alt="IMAGE ALT TEXT HERE" width="240" height="180" border="10" /></a>
```

Or, in pure Markdown, but losing the image sizing and border:

```no-highlight
[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg)](http://www.youtube.com/watch?v=YOUTUBE_VIDEO_ID_HERE)
```
</div>

<div class='col-md-6' markdown='1'>
<a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ" target="_blank" style='cursor: pointer'><img src="http://img.youtube.com/vi/VO0d6EuGpO0/0.jpg" 
alt="IMAGE ALT TEXT HERE" width="240" height="180" /></a>

Or, in pure Markdown, but losing the image sizing:

[![IMAGE ALT TEXT HERE](http://img.youtube.com/vi/VO0d6EuGpO0/0.jpg)](https://www.youtube.com/watch?v=dQw4w9WgXcQ)
{: style='cursor:pointer' target='_blank' }
</div>

---