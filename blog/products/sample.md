---
title: AD-FMCOMMS3-EBZ User Guide
permalink: /products/
---

This version (10 Feb 2017 19:56) was approved by rgetz.  
The [previously approved version](/ad-fmcomms3-ebz?rev=1481546072)
(12 Dec 2016 13:34) is available.
{: .alert .alert-success}

The AD-FMComms3-EBZ is an FMC board for the
[AD9361](http://www.analog.com/AD9361), a highly integrated RF Agile
Transceiver<sup>&trade;</sup>. While the complete chip level design package
can be found on the [the ADI web site](http://www.analog.com/ad9361_design_files).
Information on the card, and how to use it, the design package that surrounds it, and the software which
can make it work, can be found here.

The purpose of the AD-FMComms3-EBZ is to provide an RF platform to software
developers, system architects, etc, who want a single platform which operates
over a much wider tuning range (70 MHz – 6 GHz). It’s expected that the RF
performance of this platform can meet the datasheet specifications at 2.4 GHz,
but not at the entire RF tuning range that the board supports (but it will
work **much** better than the [AD-
FMCOMMS2-EBZ](/ad-
fmcomms2-ebz) over the complete RF frequency). We will provide typical
performance data for the entire range (70 MHz – 6 GHz) which is supported by
the platform. This is primarily for system investigation and bring up of
various waveforms from a software team before their custom hardware is
complete, where they want to see waveforms, but are not concerned about the
last 1dB or 1% EVM of performance.

The AD-FMComms3-EBZ board is very similar to the [AD-
FMComms2-EBZ](/ad-
fmcomms2-ebz) board with only one exception, the RX/TX RF differential to
single ended transformer. The AD-FMComms3-EBZ is more targetted for wider
tuning range applications, that is why we use the
[TCM1-63AX+](http://www.minicircuits.com/pdfs/TCM1-63AX+.pdf) from Mini-
Circuits as the RF transformer of choice. We affectionately call the
FMCOMMS3-EBZ the "Software Engineers" platform, and the FMCOMMS2-EBZ, the "RF
Engineers" platform to denote the difference.

![An image](ad9361_top_layer.png){:class="alignright"}.


1. [Purchase](http://www.analog.com/ad-fmcomms3-ebz#eb-buy)
2. [Introduction](/ad-fmcomms2-ebz/introduction)
3. [FMCOMMS3 Hardware](/ad-fmcomms3-ebz/hardware): This provides a brief description
of the AD-FMCOMMS3-EBZ board by itself, and is a good reference for those who want
to understand a little more about the board. If you just want to use the board, you
can skip this section, and come back to it when you want to incorporate the AD9361
into your product.
   1. [Hardware](/ad-fmcomms3-ebz/hardware) (including [schematics](/ad-fmcomms3-ebz/hardware#downloads))
2. [Functional Overview & Specifications](/ad-fmcomms2-ebz/hardware/functional_overview)
   3. [Characteristics & Performance](/ad-fmcomms3-ebz/hardware/card_specification)
   4. [Configuration options](/ad-fmcomms3-ebz/hardware/configuration_options)
   5. [FCC or CE certification](/ad-fmcomms2-ebz/certification)
   6. [Tuning the system](/ad-fmcomms2-ebz/hardware/tuning)
   7. [Production Testing Process](/ad-fmcomms2-ebz/testing)
4. Use the AD-FMCOMMS3-EBZ Board to better understand the AD9361
   1. [What you need to get started](/ad-fmcomms2-ebz/prerequisites)
   2. [Quick Start Guides](/ad-fmcomms2-ebz/quickstart)
      1. [Linux on ZC702, ZC706, ZED](/ad-fmcomms2-ebz/quickstart/zynq)
      2. [Linux on ZCU102](/ad-fmcomms2-ebz/quickstart/zynqmp)
      3. [Linux on KC705, VC707](/ad-fmcomms2-ebz/quickstart/microblaze)
      4. [Configure a pre-existing SD-Card](/linux-software/zynq_images#preparing_the_image)
      5. [Update the old card you received with your hardware](/linux-software/zynq_images#staying_up_to_date)
   3. Linux Applications
      1. [IIO Scope](/linux-software/iio_oscilloscope)
      2. [FMCOMMS2/3/4 Control IIO Scope Plugin](/linux-software/fmcomms2_plugin)
      3. [FMCOMMS2/3/4/5 Advanced Control IIO Scope Plugin](/linux-software/fmcomms2_advanced_plugin)
      4. [Command Line/Shell scripts](/ad-fmcomms2-ebz/software/linux/applications/shell_scripts)
   4. Push custom data into/out of the AD-FMCOMMS3-EBZ
      1. [Basic Data files and formats](/ad-fmcomms2-ebz/software/basic_iq_datafiles)
      2. [Create and analyze data files in MATLAB](/ad-fmcomms2-ebz/software/datafiles)
      3. [Stream data into/out of MATLAB](/linux-software/libiio/clients/matlab_simulink)
      4. [AD9361 libiio streaming example](/linux-software/libiio#libiio_-_ad9361_iio_streaming_example)
5. Design with the AD9361
   1. [Understanding the AD9361](/ad-fmcomms2-ebz/ad9361)
      1. [AD9361 Product page](http://www.analog.com/AD9361)
      2. [Full Datasheet and chip design package](/AD9361-Integrated-RF-Agile-Transceiver-Design-Res/fca.html)
      3. [MATLAB Filter Design Wizard for AD9361](/ad-fmcomms2-ebz/software/filters)
   2. Simulation
      1. [MathWorks SimRF Models of the AD9361](/ad-fmcomms2-ebz/software/simrf)
   3. Hardware in the Loop / How to design your own custom BaseBand
      1. MATLAB/Simulink Examples
         1. [Stream data into/out of MATLAB](/linux-software/libiio/clients/fmcomms2_3_simulink)
         2. [Beacon Frame Receiver Example](/linux-software/libiio/clients/beacon_frame_receiver_simulink#beacon_frame_receiver_example)
         3. [QPSK Transmit and Receive Example](/linux-software/libiio/clients/qpsk_example)
         4. [LTE Transmit and Receive Example](/linux-software/libiio/clients/lte_example)
         5. [ADS-B Airplane Tracking Example](/linux-software/libiio/clients/adsb_example)
      2. [GNU Radio](/linux-software/gnuradio)
      3. [FM Radio/Tuner (listen to FM signals on the HDMI monitor)](/fm-radio)
      4. [C example](/linux-software/libiio#libiio_-_ad9361_iio_streaming_example)
   4. Targeting
      1. [Analog Devices BSP for MathWorks HDL Workflow Advisor](/ad-fmcomms2-ebz/software/matlab_bsp)
   5. Complete Workflow
      1. [ADS-B Airplane Tracking Tutorial](/picozed_sdr/tutorials/adsb)
   6. Design a custom AD9361 based platform
      1. [Linux software](/ad-fmcomms2-ebz/software/linux)
         1. [Linux Device Driver](/linux-drivers/iio-transceiver/ad9361)
         2. [Build the demo on ZC702, ZC706, or ZED from source](/ad-fmcomms2-ebz/software/linux/zynq)
         3. [Build ZynqMP/MPSoC Linux kernel and devicetrees from source](/ad-fmcomms2-ebz/software/linux/zynqmp)
         4. [Build the demo on KC705 or VC707 for Microblaze from source](/ad-fmcomms2-ebz/software/linux/microblaze)
         5. [Build the 2015_R2 Release Linux kernel from source](/ad-fmcomms2-ebz/software/linux/zynq_2015r2)
         6. [Customizing the devicetree on the target](/ad-fmcomms2-ebz/software/linux/zynq_tips_tricks)
      2. [No-OS Driver](/ad-fmcomms2-ebz/software/baremetal)
      3. [HDL Reference Design](/ad-fmcomms2-ebz/reference_hdl) which you must use in your FPGA.
         1. [Digital Interface Timing Validation](/ad-fmcomms2-ebz/interface_timing_validation)
6. Additional Documentation about SDR Signal Chains
   1. [The math behind the RF](/ad-fmcomms1-ebz/math)
7. [Help and Support](/ad-fmcomms2-ebz/help_and_support)


## Warning

All the products described on this page include ESD (electrostatic discharge)
sensitive devices. Electrostatic charges as high as 4000V readily accumulate
on the human body or test equipment and can discharge without detection.\\
\\
Although the boards feature ESD protection circuitry, permanent damage may
occur on devices subjected to high-energy electrostatic discharges. Therefore,
proper ESD precautions are recommended to avoid performance degradation or
loss of functionality. This includes removing static charge on external
equipment, cables, or antennas before connecting to the device.
{: .alert .alert-warning}
